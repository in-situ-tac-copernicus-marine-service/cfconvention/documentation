
[[Timeseries, Chapter X, Timeseries format description]]

== Timeseries using Discrete Sampling Geometries

Here is a list of dataTypes that can be trajectory:

* MO: Moored buoys
* TG: Tide-gauges
* RF: River flow


Describing platform that can be timeseries using DSG that appears in cf-convention 1.8 can be tricky

Different cases can occur.

=== Nominal and GPS positions are known

image::./images/moorings/moorings-deplt-gps.jpg[]

----
dimensions:
  TIME = 1234 ; // aggregated number of times in all timeseries
  DEPLOYMENT = 3 ; // aggregated number of deployments for stations
variables:
  float LONGITUDE ; // resp. LATITUDE
    LONGITUDE:standard_name = "longitude";
    LONGITUDE:long_name = "nominal station longitude";
    LONGITUDE:units = "degrees_east";
    LONGITUDE:axis = "X"; // resp. axis="Y" for LATITUDE
  float DEPLOY_LONGITUDE (DEPLOYMENT=3);
    DEPLOY_LONGITUDE:standard_name = "longitude";
    DEPLOY_LONGITUDE:long_name = deployment longitude";
    DEPLOY_LONGITUDE:units = "degrees_east";
  float PRECISE_LONGITUDE (TIME);
    PRECISE_LONGITUDE:standard_name = "longitude";
    PRECISE_LONGITUDE:long_name = "precise station longitude";
    PRECISE_LONGITUDE:units = "degrees_east";
  float DEPH(DEPTH=5) ;
    DEPH:long_name = "vertical distance above the surface" ;
    DEPH:standard_name = "depth" ;
    DEPH:units = "m";
    DEPH:positive = "down";
    DEPH:axis = "Z";
  char STATION(name_strlen) ;
    STATION:long_name = "station name" ;
    STATION:cf_role = "timeseries_id";
  int DEPLOYMENT(DEPLOYMENT=3) ;
    DEPLOYMENT:long_name = "index of the first time after (re)deployment" ;
    DEPLOYMENT:compress="TIME";
  double TIME(TIME) ;
    TIME:standard_name = "time";
    TIME:long_name = "time of measurement" ;
    TIME:units = "days since 1950-01-01 00:00:00" ;
    TIME:axis = "T";
  float TEMP(TIME) ;
    TEMP:standard_name = “air_temperature” ;
    TEMP:units = "Celsius" ;
    TEMP:coordinates = "TIME LONGITUDE PRECISE_LONGITUDE DEPLOY_LONGITUDE station_name" ;

data:
 STATION = "44088";
 LONGITUDE = 74.841;
 DEPLOY_LONGITUDE = 74.839, 74.842, 74.841;
 DEPLOYMENT = 24537, 24654, 26691;
----


=== Only nominal positions are known

image::./images/moorings/moorings-deplt.jpg[]

----
dimensions:
  TIME = 1234 ; // aggregated number of times in all timeseries
  DEPLOYMENT = 3 ; // aggregated number of deployments for stations
variables:
  float LONGITUDE ;
    LONGITUDE:standard_name = "longitude";
    LONGITUDE:long_name = "nominal station longitude";
    LONGITUDE:units = "degrees_east";
    LONGITUDE:axis = "X"; // resp. axis="Y" for LATITUDE
  float DEPLOY_LONGITUDE (DEPLOYMENT=3);
    DEPLOY_LONGITUDE:standard_name = "longitude";
    DEPLOY_LONGITUDE:long_name = deployment longitude";
    DEPLOY_LONGITUDE:units = "degrees_east";
  string STATION ;
    STATION:long_name = "station name" ;
    STATION:cf_role = "timeseries_id";
  int DEPLOYMENT(DEPLOYMENT=3) ;
    DEPLOYMENT:long_name = "index of the first time after (re)deployment" ;
    DEPLOYMENT:compress="TIME";
  float DEPH(DEPTH=5) ;
    DEPH:long_name = "vertical distance above the surface" ;
    DEPH:standard_name = "depth" ;
    DEPH:units = "m";
    DEPH:positive = "down";
    DEPH:axis = "Z";
  double TIME(TIME) ;
    TIME:standard_name = "time";
    TIME:long_name = "time of measurement" ;
    TIME:units = "days since 1950-01-01 00:00:00" ;
    TIME:axis = "T";
  float temp(TIME) ;
    temp:standard_name = “air_temperature” ;
    temp:units = "Celsius" ;
    temp:coordinates = "TIME LONGITUDE DEPLOY_LONGITUDE station_name " ;

data:
 STATION = "44088";
 LONGITUDE = 74.841;
 DEPLOY_LONGITUDE = 74.839, 74.842, 74.841;
 DEPLOYMENT = 24537, 24654, 26691;
 row_size = 1234;
----

=== Only GPS positions are known

image::./images/moorings/moorings-gps.jpg[]

When collected data from GTS (Global Telecommunication System) there is no parameter dedicated to nominal position

Exemple of template (template OMM 315008)

----
WMO BUFR MOORED BUOY template 315008
 DATA CATEGORY                         1
 DATA SUB - CATEGORY                     0
 LOCAL DATA SUB - CATEGORY              25
        DATA DESCRIPTORS (UNEXPANDED)
    1  315008
        DATA DESCRIPTORS (EXPANDED)
     1  001087  WMO marine observing platform extended identifier
     2  001015  Station or site name
     3  002149  Type of data buoy
     4  004001  Year
     5  004002  Month
     6  004003  Day
     7  004004  Hour
     8  004005  Minute
     9  005001  Latitude (high accuracy)
    10  006001  Longitude (high accuracy)
----

In this case, the GPS position can be reported or the nominal position

Example of Korean buoy :

image::./images/moorings/moorings-gts-2dplt.jpg[]

Example of Indian buoy :

image::./images/moorings/moorings-gts-gps.jpg[]

----
dimensions:
  TIME = 1234 ; // aggregated number of times in all timeseries
variables:
  float LONGITUDE ; // last lon
    LONGITUDE:standard_name = "longitude";
    LONGITUDE:long_name = "nominal station longitude";
    LONGITUDE:units = "degrees_east";
    LONGITUDE:axis = "X"; // resp. axis="Y" for LATITUDE    
  float PRECISE_LONGITUDE (TIME);
    PRECISE_LONGITUDE:standard_name = "longitude";
    PRECISE_LONGITUDE:long_name = "precise station longitude";
    PRECISE_LONGITUDE:units = "degrees_east";
  string STATION ;
    STATION:long_name = "station name" ;
    STATION:cf_role = "timeseries_id";
  double TIME(TIME) ;
    TIME:standard_name = "time";
    TIME:long_name = "time of measurement" ;
    TIME:units = "days since 1950-01-01 00:00:00" ;
    TIME:axis = "T";
  float temp(TIME) ;
    temp:standard_name = “air_temperature” ;
    temp:units = "Celsius" ;
    temp:coordinates = "TIME LONGITUDE PRECISE_LONGITUDE station_name" ;

data:
 STATION_NAME = "44088";
 LONGITUDE = 74.841;
 DEPLOY_LONGITUDE = 74.839, 74.842, 74.841;
 DEPLOYMENT = 24537, 24654, 26691;
----


=== Position Quality control

Up to now, each position has an ancillary variable POSITION_QC which is the position quality flag.

POSITION_QC will only be used for PRECISE_LATITUDE and PRECISE_LONGITUDE

==== For the precise position:

----
   byte POSITION_QC(TIME) ;
        POSITION_QC:long_name = "Precise position quality flag" ;
----

=== DEPH position

Example of different depth for a mooring:

image::./images/moorings/moorings-depth.jpg[]

After each deployment the sensor remains at the same height.

Instead of depending of time, depth will depend on different immersion level

----
        float DEPH(DEPTH) ;
                DEPH:standard_name = "depth" ;
                DEPH:long_name = "Depth" ;
                DEPH:units = "m" ;
                DEPH:valid_min = -12000.f ;
                DEPH:valid_max = 12000.f ;
                DEPH:axis = "Z" ;
                DEPH:positive = "down" ;
                DEPH:uncertainty = " " ;
                DEPH:reference = "sea_level" ;
                DEPH:data_mode = "R" ;
                DEPH:ancillary_variables = "DEPH_QC" ;
        byte DEPH_QC(DEPTH) ;
                DEPH_QC:long_name = "Depth quality flag" ;
                DEPH_QC:conventions = "Copernicus Marine In Situ reference table 2" ;
----

