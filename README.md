# Cf Convention



This repository contains the official source of the CF metadata conventions for Copernicus In Situ TAC.
The source files are built into HTML automatically when changes are merged into this repository.


# Building the HTML

To convert the AsciiDoc files into the resulting HTML file:

1. Ensure you have [Ruby](https://www.ruby-lang.org/) installed (e.g. `sudo apt-get install ruby`)
2. Ensure you have a recent version of [Asciidoctor](http://asciidoctor.org/) installed (e.g. `gem install asciidoctor`)
3. Get hold of the AsciiDoc files in this repo (e.g. `git clone git@github.com:cf-metadata/cf-conventions.git`)
4. Build the HTML: `asciidoctor cf-conventions.adoc`

See the [GitHub help](https://help.github.com/) pages and plethora of other git/GitHub guides for more details on how to work with repos, forks, pull requests, etc.
