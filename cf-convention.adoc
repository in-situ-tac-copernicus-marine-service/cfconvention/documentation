= NetCDF In Situ TAC Conventions
Version 1.0 (draft)
:doctype: book
:pdf-folio-placement: physical
:sectanchors:
:toc: macro
:toclevels: 3

toc::[]

:numbered!:
include::pr01.adoc[]

:numbered:
include::product.adoc[]

:numbered:
include::timeseries.adoc[]

:numbered:
include::trajectory.adoc[]

:numbered:
include::trajectory-profile.adoc[]


